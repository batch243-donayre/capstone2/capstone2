const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//To check if email exits

module.exports.checkEmailExists = (request, response,next) =>{
	return User.find({email:request.body.email}).then(result =>{
		console.log(request.body.email);
		let message = ``;
		if(result.length >0){
			message = `The ${request.body.email} is already taken, please enter another Email.`
			return response.send(message);
		}
		else{
			next();
		}
	})
}

//User Registration

module.exports.registerUser = (request, response) =>{

	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	})

	return newUser.save().then(user => {
		console.log(user);
		response.send(`You are now successfully registered.`)
	}).catch(error =>{
		console.log(error);
		response.send(`Sorry ${newUser.firstName}, there was an error during the registration. Please try again!`)
	})
}


//User Login / Authentication

module.exports.loginUser = (request, response) =>{
	return User.findOne({email : request.body.email})
	.then(result =>{

		// console.log(result);

		if(result === null){
			response.send(`Your email: ${request.body.email}, is incorrect or not yet registered. Register first!`);
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				let token = auth.createAccessToken(result);
				// console.log(token);
				return response.send({accessToken: token});
			}
			else{
				return response.send(`Incorrect password, please try again!`);
			}
		}
	})
}


//Retrieve all users - this is to easily get users ID for Updating role

module.exports.getAllUsers = (request, response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	if(!userData.isAdmin){
			return response.send("Sorry! Only Admin can access this page!")
		}else{
			return User.find({}).then(result => response.send(result))
			.catch(err =>{
			response.send(err);
		})
	}
}


//To set any User To Admin Role

module.exports.updateRole = (request, response) =>{
	let token = request.headers.authorization;
	let userData = auth.decode(token);

	let idToBeUpdated = request.params.userId;

	if (userData.isAdmin) {
	 	return User.findById(idToBeUpdated).then(result =>{
	 		let update = {
	 			isAdmin:!result.isAdmin
	 	};

	 	return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document =>{
	 		document.password ="Confidential";
	 		return response.send(document)
	 	}).catch(err => response.send(err));

	 	}).catch(err => response.send(err));
	}else{
	 	return response.send("You don't have access on this page!")
	}
}



