const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/cart");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");





module.exports.getAllOrders= (request, response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	if(!userData.isAdmin){
			return response.send("Sorry! Only admin is Authorize to access this page!")
		}else{
			return Order.find({}).then(result => response.send(result))
			.catch(err =>{
			response.send(err);
		})
	}
}




module.exports.addToCart = async (request, response)=>{
	let token = request.headers.authorization;
	const userData = auth.decode(token);

	const quantity = request.body.quantity;
	
	if(!userData.isAdmin){
		const productDetails = await Product.findById(request.params.productId)
		console.log(productDetails);
		let totalAmount = quantity*productDetails.price;
		console.log(totalAmount)
		const cart = await Cart.create({
			userId:userData.id,
			productId:productDetails._id,
			productName : productDetails.productName,
			price : productDetails.price,
			quantity : quantity,
			totalAmount
		})
		return response.send(cart);
		}else{
		response.send("Sorry! You are an admin.");
		}
	
}


module.exports.checkOut = async (request, response) =>{
	let token = request.headers.authorization;
	const userData = auth.decode(token);

	const {userId} = request.params;

	if(!userData.isAdmin){
		const carts = await Cart.find({
			userId,
		})
		
		const products = [];
		   let totalAmount = 0;

		   
		   const productIds = [];
		  
		for (let i = 0; i < carts.length; i++) {
		     totalAmount += carts[i].totalAmount;
		     products.push({
		     	userId : carts[i].userId,
		       productId: carts[i].productId,
		       productName: carts[i].productName,
		       quantity: carts[i].quantity,
		       totalAmount 
		     });
		     productIds.push(carts[i].productId);
		}

		await Cart.deleteMany({ userId });

		const order = await Order.create(products);
		console.log(order);
		return response.send(order);


	}else{
		response.send("Sorry! You are an admin.");
		}

}



	