

const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";


module.exports.createAccessToken = (result) =>{

	const data = {
		id: result._id,
		email: result.email,
		isAdmin : result.isAdmin
	}
	return jwt.sign(data, secret, {});

}



module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization;
	
	if(token !== undefined){
		token = token.slice(7, token.length);
		// console.log(token);
		return jwt.verify(token, secret, (error, data)=>{
			if(error){
				return response.send("Invalid Token");
			}
			else{
				next();
			}
		})
	}
	else{
		return response.send("Authentication failed! No Token provided");
	}

}



module.exports.decode = (token) => {
	if(token === undefined){
		return null
	}else{
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return null;
			}else{
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
}


