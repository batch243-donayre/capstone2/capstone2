//Setup dependencies 
 
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');


const userRoutes = require("./routes/userRoutes");
const productsRoutes = require("./routes/productsRoutes");
const orderRoutes = require("./routes/orderRoutes");
const app = express();

//Database connection 
 mongoose.connect("mongodb+srv://admin:admin@zuittbatch243donayre.savl9qu.mongodb.net/Capstone2?retryWrites=true&w=majority", 
 					{
 						useNewUrlParser: true,
 						useUnifiedTopology:true
 	
    				})

mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once('open',() => console.log("Now connected to MongoDB Atlas"));


app.use(cors());
app.use(express());


app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/users", userRoutes);
app.use("/products", productsRoutes);
app.use("/orders", orderRoutes);



 app.listen(process.env.PORT || 4000, () =>{
 	console.log(`API is now online on port ${process.env.PORT||4000}`);
 })
