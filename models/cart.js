const mongoose = require("mongoose");
const Cart = require("../models/cart");


const cartSchema = new mongoose.Schema({

		userId : {
			type: String,
			required : [true, "User ID is required"]
		},

		productId: {
			type: String,
			required : [true, "Product ID is required"]
		},

		productName: {
			type: String,
			required : [true, "Product Name is required"]
		},

		price: {
			type: Number,
			required : [true, "Price is required"]
		},
											
		quantity: {
			type: Number,
			required : [true, "Quantity is required"]
		},

		totalAmount: {
				type: Number,
				default: 0
		}

});

module.exports = mongoose.model("Cart", cartSchema);
